const mongoose = require('mongoose')
const composeWithMongoose = require('graphql-compose-mongoose').composeWithMongoose
const ThumbnailsSchema = require('./../schemas/thumbnails')

const VideoSchema = new mongoose.Schema({
  id: String,
  title: String,
  description: String,
  thumbnails: ThumbnailsSchema,
  duration: String,
  viewCount: Number,
  channelTitle: String,
  channelId: String,
  publishedAt: Date
})
const VideoModel = mongoose.model('VideoModel', VideoSchema)

const customizationOptions = {}
const VideoTC = composeWithMongoose(VideoModel, customizationOptions)

module.exports = VideoTC
