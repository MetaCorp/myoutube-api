const mongoose = require('mongoose')
const composeWithMongoose = require('graphql-compose-mongoose').composeWithMongoose

const MtagSchema = new mongoose.Schema({
  name: String, // standard types
})
const MtagModel = mongoose.model('MtagModel', MtagSchema)

const customizationOptions = {}
const MtagTC = composeWithMongoose(MtagModel, customizationOptions)

module.exports = MtagTC