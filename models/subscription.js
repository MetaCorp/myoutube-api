const mongoose = require('mongoose')
const composeWithMongoose = require('graphql-compose-mongoose').composeWithMongoose
const ThumbnailsSchema = require('./../schemas/thumbnails')

const SubscriptionSchema = new mongoose.Schema({
  title: String,
  description: String,
  channelId: String,
  thumbnails: ThumbnailsSchema,
  publishedAt: Date
})
const SubscriptionModel = mongoose.model('SubscriptionModel', SubscriptionSchema)

const customizationOptions = {}
const SubscriptionTC = composeWithMongoose(SubscriptionModel, customizationOptions)

module.exports = SubscriptionTC
