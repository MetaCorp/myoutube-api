const { GQC } = require('graphql-compose')
const fetch = require('node-fetch')

const VideoTC = require('./models/video')

const Video = require('./videos')

const BASE_URL = 'https://www.googleapis.com/youtube/v3'
const YOUTUBE_KEY = 'AIzaSyBja-ElTLPls0Ja125lVoNtFZhcCfBzXAE'
const CHANNEL_ID = 'UCRUjV4ZUafB6RaucHuPvtgw'
const resultsPerPage = 5

const nextPageEndpoint = (pageToken) => pageToken ? `&pageToken=${pageToken}` : ''
const ytApi = {
  subcriptions: (channelId, nextPageToken) =>
    fetch(BASE_URL + `/subscriptions?part=snippet&channelId=${channelId}&maxResults=${resultsPerPage}&key=${YOUTUBE_KEY}` + nextPageEndpoint(nextPageToken)).then(res => res.json()),
  videos: (nextPageToken) =>
    fetch(BASE_URL + `/videos?part=snippet%2CcontentDetails%2Cstatistics%2CtopicDetails&maxResults=${resultsPerPage}&chart=mostPopular&key=${YOUTUBE_KEY}` + nextPageEndpoint(nextPageToken)).then(res => res.json()),
}

const fetchAllPages = (func, cb = data => data, stopCb, stopAccCb) => {
  
  const rec = (nextPageToken, acc) => func(nextPageToken).then(data => {
    if (stopCb && stopCb(data)) return stopAccCb ? [...acc, ...stopAccCb(data)] : acc
    // console.log('\nacc: ', acc.length)
    // console.log('data.nextPageToken: ', data.nextPageToken)
    // console.log('data.items: ', data.items.length)
    const newAcc = [...acc, ...cb(data)]
    return data.nextPageToken ? rec(data.nextPageToken, newAcc) : newAcc
  })

  return rec(null, [])
}

// Push to Mongo all videos
// Auth Youtube to get subcriptions
// Test stopCb

const fetchAllVideos = () =>
  fetchAllPages(nextPageToken => ytApi.videos(nextPageToken), (data) => data.items)

const fetchAllSubscriptions = (channelId) =>
  fetchAllPages(nextPageToken => ytApi.subcriptions(channelId, nextPageToken), (data) => data.items)


const getYoutubeSubscriptions = (channelId) => {
  return fetchAllVideos()
}

GQC.rootQuery().addFields({
  gqlCompose: {
    type: 'String',
    resolve: () => 'Hi from gqlCompose'
  },
  feed: {
    type: 'JSON',
    resolve: (channelId) => getYoutubeSubscriptions(channelId || CHANNEL_ID).then(data => data)
  },
  allVideos: {
    type: 'JSON',
    resolve: () => fetchAllVideos().then(data => data)
  },
  videos: {
    type: 'JSON',
    resolve: () => ytApi.videos().then(data => data)
  },
  getVideos: Video.getResolver('query'),
  videoById: VideoTC.getResolver('findById'),
  videoByIds: VideoTC.getResolver('findByIds'),
  videoOne: VideoTC.getResolver('findOne'),
  videoMany: VideoTC.getResolver('findMany'),
  videoCount: VideoTC.getResolver('count'),
  videoConnection: VideoTC.getResolver('connection')
})

module.exports = GQC.buildSchema()