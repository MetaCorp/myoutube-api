const mongoose = require('mongoose')

const ThumbnailsSchema = new mongoose.Schema({
  default: {
    url: String
  },
  medium: {
    url: String
  },
  height: {
    url: String
  },
  standardt: {
    url: String
  },
  maxres: {
    url: String
  }
})

module.exports = ThumbnailsSchema
